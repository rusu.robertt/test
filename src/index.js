import "./style/global.scss";

import React from "react";
import ReactDOM from "react-dom";
import immer from "immer";

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return null;
    }
}

ReactDOM.render(
    <Index/>,
    document.getElementById("root")
);